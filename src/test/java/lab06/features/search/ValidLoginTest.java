package lab06.features.search;

import lab06.steps.serenity.*;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Pending;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;



//acest fisier se executa
@RunWith(SerenityRunner.class)
public class ValidLoginTest {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    //vom folosi pasii corespunzatori paginii de login si account
    @Steps
    public LoginSteps login;

    @Steps
    public AccountSteps account;

    @Steps
    public NewDirSteps newDir;

    @Steps
    public DeleteDirSteps delDir;

    @Issue("#login_createDir_delete_logout")
    @Test
    public void validLogintest() {
        //pornim de la pagina de login
        login.is_the_home_page();
        //se introduc toate datele si se da clickpe butonul de Login
        login.enters_data_and_click_login("linux.scs.ubbcluj.ro","vvta","2019vvta");
        //verificam ca am ajuns in coontul utilizatorului vvta
        account.should_see_username("vvta");
        //click new dir
        account.click_new_dir_button();

        //Verificam ca s-a am ajuns pe pagina care trebuie
        newDir.should_see_newDirForm();

        newDir.enter_dir_name("director3");
        newDir.click_submitButton();

        //Verificam daca avem confirmare
        newDir.should_see_confirmation();

        newDir.click_backButton();

        account.should_see_dir("director3");

        account.select_dir_and_delete("director3");

        delDir.should_delete_dir();

        delDir.should_return();

        //In acest pas verificam ca directorul sters nu se afla in lista
        account.should_not_see_dir("director3");

        //se iese din cont
        account.click_logout();
    }

}