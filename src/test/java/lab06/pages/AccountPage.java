package lab06.pages;


import com.gargoylesoftware.htmlunit.html.DomNode;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.sql.SQLOutput;
import java.util.List;
import java.util.stream.Collectors;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.tagName;

//corespunde paginii care ii arata utilizatorului contul sau
public class AccountPage<findby> extends PageObject {

    //vom interactiona cu butonul de logout si vom verifica daca am ajuns in aceasta pagina

    //butonul de logout il identificam dupa xpath deoarece nu am gasit pentru el un mijloc mai simplu
    @FindBy(xpath = "//*[@id=\"StatusbarForm\"]/a[3]/img")
    private WebElementFacade logoutButton;

    //am mai găsit 2 elemente mai usor de identificat care contin numele utilizatorului, de exemplu, TextBox-ul care contine numele directorului in care ne aflam, situat in artea e stanga sus a paginii
    //nu putem sa ne folosim de nume in identificarea sa, deoarece nu e unicul element cu numele "directory", de aceea folosim si pentru acesta xpath
    @FindBy(xpath = "//*[@id=\"toptable\"]/tbody/tr/td[2]/input")
    private WebElementFacade currentDir;

    @FindBy(xpath = "/html/body/div/div[2]/div/form/table[2]/tbody/tr[1]/td/table/tbody/tr/td[1]/input[1]")
    private WebElementFacade newDirButton;

    @FindBy(xpath = "/html/body/div/div[2]/div/form/table[2]/tbody/tr[1]/td/table/tbody/tr/td[2]/input[3]")
    private WebElementFacade deleteButton;


    //pe butonul de logout vom da click
    public void click_logout() {
        logoutButton.click();
    }

    //vom scoate valoarea directorului curent, pentru a verifica in steps daca ea se termina cu numele utilizatorului
    public String get_currentDir() {
        return currentDir.getValue();
    }

    public void click_new_dir_button() {
        newDirButton.click();
    }

    public void click_delete_button() {
        deleteButton.click();
    }

    public boolean new_dir_in_table(String dir_name) {
        boolean result;
        if (dirTable.containsText(dir_name)) {
            return result = true;
        }
        return result = false;
    }


    @FindBy(id = "maintable")
    private WebElementFacade dirTable;


    private void click_element_by_attribute_value(WebElementFacade el, String attribute, String value) {
        List<WebElement> list = el.findElements(By.tagName("input"));
        list.forEach(System.out::println);
        for (int i = 0; i < list.size(); i++) {
            String dir = list.get(i).getAttribute(attribute);
            if (dir.equalsIgnoreCase(value)) {
                list.get(i).click();
                break;
            }
        }
    }


    public void check_directory_to_delete(String dir) {
        click_element_by_attribute_value(dirTable, "value", dir);
    }

}






//    @FindBy(tagName = "td")
//    //private WebElementFacade table_data;
//    private List<WebElementFacade> table_data;


//
//    public void find_and_click_checkbox (String dir){
//        String current_dir;
//        WebElementFacade checkbox;
//
//        for(int i=6; i<rows.size(); i++) {
//            current_dir = String.valueOf(rows.get(i).findElements(cssSelector("td:nth-child(3)")));
//            checkbox = rows.get(i).find(cssSelector("td:nth-child(1)"));
//            if (current_dir == dir){
//                checkbox.click();
//            }
//            break;
//            }
//    }
//}

//            dirname = (WebElementFacade) current_row.findElement(cssSelector("td:nth-child(3)"));
//            checkbox = (WebElementFacade) current_row.findElement(By.cssSelector("td:nth-child(1)"));

//            dirname = current_row.findElement(By.linkText("td:nth-child(3)"));
//            checkbox = current_row.findElement(By.cssSelector("td:nth-child(1)"));

//            System.out.println(dirname);
//
//            if (dirname.getText() == dir) {
//                checkbox.click();
//            }
//            break;
//        }
//    }



//    public List<String> get_list() {
//        WebElementFacade checkbox = find(tagName("td"););
//        return checkbox.findElements(tagName("td")).stream()
//                .map( element -> element.getid() )
//                .collect(Collectors.toList());
//    }


// lista dir:
//accountPage.dirTable.findElements(By.tagName("tr")
//#row2 > td:nth-child(3)

//accountPage.dirTable.findElements(By.tagName("tr")).get(4).findElement(By.cssSelector("td:nth-child(1) input")).click()

