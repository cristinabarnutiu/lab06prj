package lab06.pages;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

//corespunde paginii unde se creeaza un nou director
public class DeleteDirPage<findby> extends PageObject {

    @FindBy(xpath = "/html/body/div/div[2]/div/div[2]/form/a[2]/img")
    private WebElementFacade submitButton;



    @FindBy (xpath = "//*[@id=\"CopyMoveDeleteForm\"]/a/img")
    private WebElementFacade backButton;



    //confirmam stergerea
    public void click_submit() {
        submitButton.click();
    }

    //go back
    public void click_back() {
        backButton.click();
    }



}