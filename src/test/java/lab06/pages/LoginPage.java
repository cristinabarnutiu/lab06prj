package lab06.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

//aceasta este pagina de pornire, avem nevoie de adresa ei
@DefaultUrl("https://www.cs.ubbcluj.ro/apps/ftp/")
public class LoginPage extends PageObject {

    //trebuie selectam serverul ftp, sa completam numele, parola si sa apasam butonul de Login
    //asadar com crea cate un element pentru fiecare

    //butonul de Login il identificam dupa nume
    @FindBy(name="Login")
    private WebElementFacade loginButton;

    //textBox-ul in care completam parola il identificam dupa nume
    @FindBy(name="password")
    private WebElementFacade passwordTextBox;

    //textBox-ul in care completam numele de utilizator il identificam dupa nume
    @FindBy(name="username")
    private WebElementFacade usernameTextBox;

    //DropBox-ul din care selectam serverul ftp il identificam dupa nume
    @FindBy(name="ftpserver")
    private WebElementFacade ftpserverDropBox;

    //vom defini metode pentru interactiunea simpla cu aceste elemente ale paginii de Login

    //in textbox-ul cu numele de utilizator vom scrie numele
    public void enter_username(String name) {
        usernameTextBox.type(name);
    }

    //in textbox-ul cu parola vom scrie parola
    public void enter_password(String pass) {
        passwordTextBox.type(pass);
    }

    //din DropDownBox-ul cu serverul ftp vom selecta o valoare
    public void select_ftpserver(String ftp) {
        ftpserverDropBox.selectByValue(ftp);
    }

    //vom da un click pe DropDownBox-ul cu serverul ftp
    public void click_ftpserver() { ftpserverDropBox.click(); }

    //pe butonul de Login vom da un click
    public void click_login_button() {
        loginButton.click();
    }

}