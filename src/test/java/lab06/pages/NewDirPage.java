package lab06.pages;


import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.pages.PageObject;

//corespunde paginii unde se creeaza un nou director
public class NewDirPage<findby> extends PageObject {


    @FindBy(xpath = "//html/body/div/div[2]/div/div[2]/form/input[16]")
    private WebElementFacade newDirTextBox;

    @FindBy(id = "NewDirForm")
    private WebElementFacade newDirForm;

    @FindBy (xpath = "//*[@id=\"NewDirForm\"]/a[2]/img")
    private WebElementFacade submitButton;

    @FindBy (xpath = "//*[@id=\"NewDirForm\"]/a/img")
    private WebElementFacade backButton;

    //vom introduce numele directorului
    public void enter_dir_name(String name) {
        newDirTextBox.type(name);
    }

    //metoda care verifica daca am ajuns la pagina cu formularul new dir
    public boolean is_newDirForm_present (){
        boolean result;
        if (newDirForm.isPresent()){
            return result = true;
        }
        return result = false;
    }

    //metoda care da click pe submit
    public void click_submitButton() {
        submitButton.click();
    }

    //metoda care verifica daca s-a creat dir
    public boolean is_newDir_created (){
        boolean result;
        if (newDirForm.containsText("was successfully created")){
            return result = true;
        }
        return result = false;

    }

    //metoda care da click pe back
    public void click_backButton() {
        backButton.click();
    }

}