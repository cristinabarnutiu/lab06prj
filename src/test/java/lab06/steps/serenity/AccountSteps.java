package lab06.steps.serenity;

import lab06.pages.AccountPage;
import lab06.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import org.jruby.RubyBoolean;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

//in aceasta clasa definim pasii de urmat in pagina contului utilizatorului
public class AccountSteps {

    //avem nevoie de pagina utilizatorului
    AccountPage accountPage;

    //un pas ar fi verificarea daca directorul curent se termina cu numele utilizatorului
    @Step
    public void should_see_username(String username) {
        //am folosit urmatoarea linie in debug pentru a vedea ce valoare are Directorul curent
        String s=accountPage.get_currentDir();
        assertTrue(accountPage.get_currentDir().endsWith(username));
    }

    //un alt pas e cel in care utilizatorul se delogheaza
    @Step
    public void click_logout() {
        accountPage.click_logout();
    }

    @Step
    public void is_the_home_page() {accountPage.open();
    }

    @Step
    public void click_new_dir_button() {accountPage.click_new_dir_button();
    }


    @Step
    public void should_see_dir(String dir) {
        assertTrue(accountPage.new_dir_in_table(dir));
    }

    @Step
    public void should_not_see_dir(String dir) {
        assertFalse(accountPage.new_dir_in_table(dir));
    }

    @Step
    public void select_dir_and_delete(String dir) {
        accountPage.check_directory_to_delete(dir);
        accountPage.click_delete_button();
    }
}