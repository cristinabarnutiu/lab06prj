package lab06.steps.serenity;

import lab06.pages.DeleteDirPage;
import lab06.pages.NewDirPage;
import net.thucydides.core.annotations.Step;

import static org.junit.Assert.assertTrue;

//in aceasta clasa definim pasii de urmat in pagina new dir
public class DeleteDirSteps {

    DeleteDirPage deleteDirPage;

    @Step
    public void should_delete_dir() {
        deleteDirPage.click_submit();
    }


    @Step
    public void should_return() {
        deleteDirPage.click_back();
    }

}