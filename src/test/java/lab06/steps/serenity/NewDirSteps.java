package lab06.steps.serenity;

import lab06.pages.AccountPage;
import lab06.pages.NewDirPage;
import net.thucydides.core.annotations.Step;

import static org.junit.Assert.assertTrue;

//in aceasta clasa definim pasii de urmat in pagina new dir
public class NewDirSteps {

    NewDirPage newDirPage;

    @Step
    public void should_see_newDirForm() {
      assertTrue(newDirPage.is_newDirForm_present());
    }


    @Step
    public void enter_dir_name (String dir_name){
        newDirPage.enter_dir_name(dir_name);
    }

    @Step
    public void click_submitButton (){
        newDirPage.click_submitButton();
    }

    @Step
    public void should_see_confirmation() {
        assertTrue(newDirPage.is_newDir_created());
    }

    @Step
    public void click_backButton (){
        newDirPage.click_backButton();
    }
}